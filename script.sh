#! /usr/bin/env bash

# Install repo Docker
echo "----- Install repo Docker ------"
apt-get install ca-certificates curl gnupg lsb-release -y
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Install Docker packages
echo "------ Install Docker packages ------"
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

# Manage Docker as a non-root user
echo "------ Manage Docker as a non-root user ------"
groupadd docker
usermod -aG docker vagrant
newgrp docker

# Add the official GitLab repository
echo "------ Add the official GitLab repository ------"
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash

# Install gitlab-runner
echo "------ Install gitlab-runner ------"
apt-get install gitlab-runner

# Register gitlab-runner
echo "------ Register gitlab-runners ------"
gitlab-runner register \
  --non-interactive \
  --url $url \
  --registration-token $TOKEN \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner-from-vagrant" \
  --maintenance-note "From vagrant Ubuntu2204" \
  --tag-list "docker" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
  
gitlab-runner register \
  --non-interactive \
  --url $url \
  --registration-token $TOKEN \
  --executor "shell" \
  --description "shell-runner-from-vagrant" \
  --maintenance-note "From vagrant Ubuntu2204" \
  --tag-list "shell" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"